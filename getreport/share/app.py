import tempfile, csv, os
from datetime import datetime, timedelta

from googleads import dfp, errors


GOOGLEADS_YAML_PATH = './googleads.yaml'
EXPORT_FILE_DIR     = './result'

# Set the start and end dates of the report to run (past 8 days).
end_date = datetime.now().date()
start_date = end_date - timedelta(days=8)

# Create report job.
# dimensions:
# https://developers.google.com/doubleclick-publishers/docs/reference/v201708/ReportService.ReportQuery
# columns:
# https://developers.google.com/doubleclick-publishers/docs/reference/v201708/ReportService.Column
report_job = {
    'reportQuery': {
        'dimensions': [
            'DATE',
            'AD_UNIT_NAME',
            'ADVERTISER_NAME'
        ],
        'dimensionAttributes': [],
        'columns': [
            'AD_SERVER_IMPRESSIONS',
            'AD_SERVER_DOWNLOADED_IMPRESSIONS',
            #'AD_SERVER_TARGETED_IMPRESSIONS',
            'AD_SERVER_CLICKS',
            #'AD_SERVER_TARGETED_CLICKS',
            'AD_SERVER_CPM_AND_CPC_REVENUE',
            #'AD_SERVER_CPD_REVENUE',
            #'AD_SERVER_CPA_REVENUE',
            'AD_SERVER_ALL_REVENUE',
        ],
        'dateRangeType': 'CUSTOM_DATE',
        'startDate': start_date,
        'endDate': end_date
    }
}

# Initialize client object.
client = dfp.DfpClient.LoadFromStorage(path=GOOGLEADS_YAML_PATH)

# Initialize a DataDownloader.
report_downloader = client.GetDataDownloader(version='v201708')

try:
    # Run the report and wait for it to finish.
    report_job_id = report_downloader.WaitForReport(report_job)
except errors.DfpReportError as e:
    print('Failed to generate report. Error was: %s' % e)


# Change to your preferred export format.
export_format = 'CSV_DUMP'

report_file = tempfile.NamedTemporaryFile(suffix='.csv', dir=EXPORT_FILE_DIR, delete=False)

# Download report data., delete=Falsi
report_downloader.DownloadReportToFile(
    report_job_id, export_format, report_file, use_gzip_compression=False)
report_file.close()

os.chmod(report_file.name, 438)

# Display results.
print('Report job with id \'%s\' downloaded to:\n%s' % (
    report_job_id, report_file.name))
